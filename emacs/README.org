#+TITLE: Emacs Configuration
#+AUTHOR: KSP Atlas

Move this config to ~/.emacs.d/configuration.org, then add (org-babel-load-file "~/.emacs.d/configuration.org") into your init file.

* Variables
** Disable GUI components
This code will disable the menu bar, tool bar, scroll bar and splash screen, turning emacs GUI into a "graphical terminal".

#+NAME: Disable GUI components
#+BEGIN_SRC emacs-lisp
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-splash-screen t)
#+END_SRC

** Set custom.el save location
This code will set the variable custom-file to another file, and load it.

#+NAME: Set custom.el save location
#+BEGIN_SRC emacs-lisp
(setq custom-file "/home/ksp/.emacs.d/autoconf.el")
(load "/home/ksp/.emacs.d/autoconf.el")
#+END_SRC
* Packages
** Load straight.el
This code checks to see if straight.el is installed. If not, bootstrap it.

#+NAME: Load straight.el
#+BEGIN_SRC emacs-lisp
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
#+END_SRC
** Setup use-package
*** Install use-package
This code will install use-package is it is not installed.

#+NAME: Install use-package
#+BEGIN_SRC emacs-lisp
(straight-use-package 'use-package)
#+END_SRC
*** Create use-pkg macro
This code will create a macro called use-pkg, which automatically sets up use-package to use straight.el.

#+NAME: Create use-pkg macro
#+BEGIN_SRC emacs-lisp
;; Macro for packages
(defmacro use-pkg (pkg &rest rest)
  `(use-package ,pkg :straight t ,@rest))
#+END_SRC
** List of packages
This code will install/update a package if it hasn't already, and it sets up a config.

#+NAME: List of packages
#+BEGIN_SRC emacs-lisp
  (use-pkg xkcd)
  (use-pkg base16-theme)
  (use-pkg slime :init (setq inferior-lisp-program "sbcl"))
  (use-pkg elpher)
  (use-pkg rainbow-delimiters)
  (use-pkg mastodon :init (progn
                                (setq mastodon-instance-url "https://social.linux.pizza/")
                                (setq mastodon-active-user "kspatlas")))
  (use-pkg lispy :config (add-hook 'emacs-lisp-mode-hook (lambda () (lispy-mode 1))))
  (use-pkg magit)
  (use-pkg haskell-mode)
#+END_SRC
* Theming
** Theme
This code will load the theme base16-monokai from the base16-theme package.
#+NAME: Theme
#+BEGIN_SRC emacs-lisp
(load-theme 'base16-monokai t)
#+END_SRC
** Font
This code will set the font to JetBrains Mono, assuming you have it installed.
#+NAME: Font
#+BEGIN_SRC emacs-lisp
(add-to-list 'default-frame-alist '(font . "JetBrains Mono-10"))
#+END_SRC

* Generic Config
** Org indent mode
This code will automatically load org-indent-mode.
#+NAME: Org indent mode
#+BEGIN_SRC emacs-lisp
(add-hook 'org-mode-hook (lambda () (org-indent-mode 1)))
#+END_SRC
